#!/bin/bash

# Find all .yml files and store them in an array
FILES=$(find . -type f -name "*.yml")

# Variable to keep track of errors
ERROR_FOUND=false

# Check each file for backslashes in the content
for file in $FILES; do
  if grep -q '\\\\' "$file"; then
    echo "Error: Backslashes are not allowed in the file: $file"
    ERROR_FOUND=true
  fi
done

# If any error is found, exit with a non-zero code to fail the pipeline
if $ERROR_FOUND; then
  exit 1
fi

echo "All files are in the correct format."
exit 0
